/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_CONST_ASCIICHARS_HPP
#define OCL_GUARD_CONST_ASCIICHARS_HPP

namespace ocl
{

template<typename char_type>
struct ASCIIChars;

template<>
struct ASCIIChars<char>
{
    typedef char char_type;

    static char_type const CHAR_NULL                 = '\0';
    static char_type const CHAR_SOH                  = '\x01'; // Start of heading
    static char_type const CHAR_STX                  = '\x02'; // start of text
    static char_type const CHAR_ETX                  = '\x03'; // end of text
    static char_type const CHAR_EOT                  = '\x04'; // end of transmission
    static char_type const CHAR_ENQ                  = '\x05'; // enquiry
    static char_type const CHAR_ACK                  = '\x06'; // acknowledge
    static char_type const CHAR_BEL                  = '\x07'; // bell
    static char_type const CHAR_BS                   = '\x08'; // backspace
    static char_type const CHAR_TAB                  = '\x09'; // horizontal tab
    static char_type const CHAR_LF                   = '\n';   // line feed/new line
    static char_type const CHAR_VT                   = '\x0b'; // vertical tab
    static char_type const CHAR_FF                   = '\x0c'; // NP form feed, new page
    static char_type const CHAR_CR                   = '\r';   // carriage return
    static char_type const CHAR_SO                   = '\x0e'; // shift out
    static char_type const CHAR_SI                   = '\x0f'; // shift in
    static char_type const CHAR_DLE                  = '\x10'; // data link escape
    static char_type const CHAR_DC1                  = '\x11'; // device control 1
    static char_type const CHAR_DC2                  = '\x12'; // device control 2
    static char_type const CHAR_DC3                  = '\x13'; // device control 3
    static char_type const CHAR_DC4                  = '\x14'; // device control 4
    static char_type const CHAR_NAK                  = '\x15'; // negative acknowledge
    static char_type const CHAR_SYN                  = '\x16'; // synchronous idle
    static char_type const CHAR_ETB                  = '\x17'; // end of trans. block
    static char_type const CHAR_CAN                  = '\x18'; // cancel
    static char_type const CHAR_EM                   = '\x19'; // end of medium
    static char_type const CHAR_SUB                  = '\x1a'; // substitute
    static char_type const CHAR_ESC                  = '\x1b'; // escape
    static char_type const CHAR_FS                   = '\x1c'; // file separator
    static char_type const CHAR_GS                   = '\x1d'; // group separator
    static char_type const CHAR_RS                   = '\x1e'; // record separator
    static char_type const CHAR_US                   = '\x1f'; // unit separator
    static char_type const CHAR_SPACE                = ' ';
    static char_type const CHAR_EXCLAMATION_MARK     = '!';
    static char_type const CHAR_QUOTATION_MARK       = '\"';
    static char_type const CHAR_HASH                 = '#';
    static char_type const CHAR_DOLLAR               = '$';
    static char_type const CHAR_PERCENT              = '%';
    static char_type const CHAR_AMPERSAND            = '&';
    static char_type const CHAR_APOSTROPHE           = '\'';
    static char_type const CHAR_OPEN_PARENTHESIS     = '(';
    static char_type const CHAR_CLOSE_PARENTHESIS    = ')';
    static char_type const CHAR_ASTERISK             = '*';
    static char_type const CHAR_PLUS                 = '+';
    static char_type const CHAR_COMMA                = ',';
    static char_type const CHAR_HYPHEN               = '-';
    static char_type const CHAR_PERIOD               = '.';
    static char_type const CHAR_FORWARD_SLASH        = '/';
    static char_type const CHAR_ZERO                 = '0';
    static char_type const CHAR_ONE                  = '1';
    static char_type const CHAR_TWO                  = '2';
    static char_type const CHAR_THREE                = '3';
    static char_type const CHAR_FOUR                 = '4';
    static char_type const CHAR_FIVE                 = '5';
    static char_type const CHAR_SIX                  = '6';
    static char_type const CHAR_SEVEN                = '7';
    static char_type const CHAR_EIGHT                = '8';
    static char_type const CHAR_NINE                 = '9';
    static char_type const CHAR_COLON                = ':';
    static char_type const CHAR_SEMICOLON            = ';';
    static char_type const CHAR_LESS_THAN            = '<';
    static char_type const CHAR_EQUAL                = '=';
    static char_type const CHAR_GREATER_THAN         = '>';
    static char_type const CHAR_QUESTION_MARK        = '?';
    static char_type const CHAR_AT                   = '@';
    static char_type const CHAR_UPPERCASE_A          = 'A';
    static char_type const CHAR_UPPERCASE_B          = 'B';
    static char_type const CHAR_UPPERCASE_C          = 'C';
    static char_type const CHAR_UPPERCASE_D          = 'D';
    static char_type const CHAR_UPPERCASE_E          = 'E';
    static char_type const CHAR_UPPERCASE_F          = 'F';
    static char_type const CHAR_UPPERCASE_G          = 'G';
    static char_type const CHAR_UPPERCASE_H          = 'H';
    static char_type const CHAR_UPPERCASE_I          = 'I';
    static char_type const CHAR_UPPERCASE_J          = 'J';
    static char_type const CHAR_UPPERCASE_K          = 'K';
    static char_type const CHAR_UPPERCASE_L          = 'L';
    static char_type const CHAR_UPPERCASE_M          = 'M';
    static char_type const CHAR_UPPERCASE_N          = 'N';
    static char_type const CHAR_UPPERCASE_O          = 'O';
    static char_type const CHAR_UPPERCASE_P          = 'P';
    static char_type const CHAR_UPPERCASE_Q          = 'Q';
    static char_type const CHAR_UPPERCASE_R          = 'R';
    static char_type const CHAR_UPPERCASE_S          = 'S';
    static char_type const CHAR_UPPERCASE_T          = 'T';
    static char_type const CHAR_UPPERCASE_U          = 'U';
    static char_type const CHAR_UPPERCASE_V          = 'V';
    static char_type const CHAR_UPPERCASE_W          = 'W';
    static char_type const CHAR_UPPERCASE_X          = 'X';
    static char_type const CHAR_UPPERCASE_Y          = 'Y';
    static char_type const CHAR_UPPERCASE_Z          = 'Z';
    static char_type const CHAR_OPEN_SQUARE_BRACKET  = '[';
    static char_type const CHAR_BACK_SLASH           = '\\';
    static char_type const CHAR_CLOSE_SQUARE_BRACKET = ']';
    static char_type const CHAR_CARET                = '^';
    static char_type const CHAR_UNDERSCORE           = '_';
    static char_type const CHAR_GRAVE_ACCENT         = '`';
    static char_type const CHAR_LOWERCASE_A          = 'a';
    static char_type const CHAR_LOWERCASE_B          = 'b';
    static char_type const CHAR_LOWERCASE_C          = 'c';
    static char_type const CHAR_LOWERCASE_D          = 'd';
    static char_type const CHAR_LOWERCASE_E          = 'e';
    static char_type const CHAR_LOWERCASE_F          = 'f';
    static char_type const CHAR_LOWERCASE_G          = 'g';
    static char_type const CHAR_LOWERCASE_H          = 'h';
    static char_type const CHAR_LOWERCASE_I          = 'i';
    static char_type const CHAR_LOWERCASE_J          = 'j';
    static char_type const CHAR_LOWERCASE_K          = 'k';
    static char_type const CHAR_LOWERCASE_L          = 'l';
    static char_type const CHAR_LOWERCASE_M          = 'm';
    static char_type const CHAR_LOWERCASE_N          = 'n';
    static char_type const CHAR_LOWERCASE_O          = 'o';
    static char_type const CHAR_LOWERCASE_P          = 'p';
    static char_type const CHAR_LOWERCASE_Q          = 'q';
    static char_type const CHAR_LOWERCASE_R          = 'r';
    static char_type const CHAR_LOWERCASE_S          = 's';
    static char_type const CHAR_LOWERCASE_T          = 't';
    static char_type const CHAR_LOWERCASE_U          = 'u';
    static char_type const CHAR_LOWERCASE_V          = 'v';
    static char_type const CHAR_LOWERCASE_W          = 'w';
    static char_type const CHAR_LOWERCASE_X          = 'x';
    static char_type const CHAR_LOWERCASE_Y          = 'y';
    static char_type const CHAR_LOWERCASE_Z          = 'z';
    static char_type const CHAR_OPEN_CURLY__BRACE    = '{';
    static char_type const CHAR_CLOSE_CURLY__BRACE   = '}';
    static char_type const CHAR_DELETE               = '\x7f';
};

template<>
struct ASCIIChars<wchar_t>
{
    typedef wchar_t char_type;

    static char_type const CHAR_NULL                 = L'\0';
    static char_type const CHAR_SOH                  = L'\x01'; // Start of heading
    static char_type const CHAR_STX                  = L'\x02'; // start of text
    static char_type const CHAR_ETX                  = L'\x03'; // end of text
    static char_type const CHAR_EOT                  = L'\x04'; // end of transmission
    static char_type const CHAR_ENQ                  = L'\x05'; // enquiry
    static char_type const CHAR_ACK                  = L'\x06'; // acknowledge
    static char_type const CHAR_BEL                  = L'\x07'; // bell
    static char_type const CHAR_BS                   = L'\x08'; // backspace
    static char_type const CHAR_TAB                  = L'\x09'; // horizontal tab
    static char_type const CHAR_LF                   = L'\n';   // line feed/new line
    static char_type const CHAR_VT                   = L'\x0b'; // vertical tab
    static char_type const CHAR_FF                   = L'\x0c'; // NP form feed, new page
    static char_type const CHAR_CR                   = L'\r';   // carriage return
    static char_type const CHAR_SO                   = L'\x0e'; // shift out
    static char_type const CHAR_SI                   = L'\x0f'; // shift in
    static char_type const CHAR_DLE                  = L'\x10'; // data link escape
    static char_type const CHAR_DC1                  = L'\x11'; // device control 1
    static char_type const CHAR_DC2                  = L'\x12'; // device control 2
    static char_type const CHAR_DC3                  = L'\x13'; // device control 3
    static char_type const CHAR_DC4                  = L'\x14'; // device control 4
    static char_type const CHAR_NAK                  = L'\x15'; // negative acknowledge
    static char_type const CHAR_SYN                  = L'\x16'; // synchronous idle
    static char_type const CHAR_ETB                  = L'\x17'; // end of trans. block
    static char_type const CHAR_CAN                  = L'\x18'; // cancel
    static char_type const CHAR_EM                   = L'\x19'; // end of medium
    static char_type const CHAR_SUB                  = L'\x1a'; // substitute
    static char_type const CHAR_ESC                  = L'\x1b'; // escape
    static char_type const CHAR_FS                   = L'\x1c'; // file separator
    static char_type const CHAR_GS                   = L'\x1d'; // group separator
    static char_type const CHAR_RS                   = L'\x1e'; // record separator
    static char_type const CHAR_US                   = L'\x1f'; // unit separator
    static char_type const CHAR_SPACE                = L' ';
    static char_type const CHAR_EXCLAMATION_MARK     = L'!';
    static char_type const CHAR_QUOTATION_MARK       = L'\"';
    static char_type const CHAR_HASH                 = L'#';
    static char_type const CHAR_DOLLAR               = L'$';
    static char_type const CHAR_PERCENT              = L'%';
    static char_type const CHAR_AMPERSAND            = L'&';
    static char_type const CHAR_APOSTROPHE           = L'\'';
    static char_type const CHAR_OPEN_PARENTHESIS     = L'(';
    static char_type const CHAR_CLOSE_PARENTHESIS    = L')';
    static char_type const CHAR_ASTERISK             = L'*';
    static char_type const CHAR_PLUS                 = L'+';
    static char_type const CHAR_COMMA                = L',';
    static char_type const CHAR_HYPHEN               = L'-';
    static char_type const CHAR_PERIOD               = L'.';
    static char_type const CHAR_FORWARD_SLASH        = L'/';
    static char_type const CHAR_ZERO                 = L'0';
    static char_type const CHAR_ONE                  = L'1';
    static char_type const CHAR_TWO                  = L'2';
    static char_type const CHAR_THREE                = L'3';
    static char_type const CHAR_FOUR                 = L'4';
    static char_type const CHAR_FIVE                 = L'5';
    static char_type const CHAR_SIX                  = L'6';
    static char_type const CHAR_SEVEN                = L'7';
    static char_type const CHAR_EIGHT                = L'8';
    static char_type const CHAR_NINE                 = L'9';
    static char_type const CHAR_COLON                = L':';
    static char_type const CHAR_SEMICOLON            = L';';
    static char_type const CHAR_LESS_THAN            = L'<';
    static char_type const CHAR_EQUAL                = L'=';
    static char_type const CHAR_GREATER_THAN         = L'>';
    static char_type const CHAR_QUESTION_MARK        = L'?';
    static char_type const CHAR_AT                   = L'@';
    static char_type const CHAR_UPPERCASE_A          = L'A';
    static char_type const CHAR_UPPERCASE_B          = L'B';
    static char_type const CHAR_UPPERCASE_C          = L'C';
    static char_type const CHAR_UPPERCASE_D          = L'D';
    static char_type const CHAR_UPPERCASE_E          = L'E';
    static char_type const CHAR_UPPERCASE_F          = L'F';
    static char_type const CHAR_UPPERCASE_G          = L'G';
    static char_type const CHAR_UPPERCASE_H          = L'H';
    static char_type const CHAR_UPPERCASE_I          = L'I';
    static char_type const CHAR_UPPERCASE_J          = L'J';
    static char_type const CHAR_UPPERCASE_K          = L'K';
    static char_type const CHAR_UPPERCASE_L          = L'L';
    static char_type const CHAR_UPPERCASE_M          = L'M';
    static char_type const CHAR_UPPERCASE_N          = L'N';
    static char_type const CHAR_UPPERCASE_O          = L'O';
    static char_type const CHAR_UPPERCASE_P          = L'P';
    static char_type const CHAR_UPPERCASE_Q          = L'Q';
    static char_type const CHAR_UPPERCASE_R          = L'R';
    static char_type const CHAR_UPPERCASE_S          = L'S';
    static char_type const CHAR_UPPERCASE_T          = L'T';
    static char_type const CHAR_UPPERCASE_U          = L'U';
    static char_type const CHAR_UPPERCASE_V          = L'V';
    static char_type const CHAR_UPPERCASE_W          = L'W';
    static char_type const CHAR_UPPERCASE_X          = L'X';
    static char_type const CHAR_UPPERCASE_Y          = L'Y';
    static char_type const CHAR_UPPERCASE_Z          = L'Z';
    static char_type const CHAR_OPEN_SQUARE_BRACKET  = L'[';
    static char_type const CHAR_BACK_SLASH           = L'\\';
    static char_type const CHAR_CLOSE_SQUARE_BRACKET = L']';
    static char_type const CHAR_CARET                = L'^';
    static char_type const CHAR_UNDERSCORE           = L'_';
    static char_type const CHAR_GRAVE_ACCENT         = L'`';
    static char_type const CHAR_LOWERCASE_A          = L'a';
    static char_type const CHAR_LOWERCASE_B          = L'b';
    static char_type const CHAR_LOWERCASE_C          = L'c';
    static char_type const CHAR_LOWERCASE_D          = L'd';
    static char_type const CHAR_LOWERCASE_E          = L'e';
    static char_type const CHAR_LOWERCASE_F          = L'f';
    static char_type const CHAR_LOWERCASE_G          = L'g';
    static char_type const CHAR_LOWERCASE_H          = L'h';
    static char_type const CHAR_LOWERCASE_I          = L'i';
    static char_type const CHAR_LOWERCASE_J          = L'j';
    static char_type const CHAR_LOWERCASE_K          = L'k';
    static char_type const CHAR_LOWERCASE_L          = L'l';
    static char_type const CHAR_LOWERCASE_M          = L'm';
    static char_type const CHAR_LOWERCASE_N          = L'n';
    static char_type const CHAR_LOWERCASE_O          = L'o';
    static char_type const CHAR_LOWERCASE_P          = L'p';
    static char_type const CHAR_LOWERCASE_Q          = L'q';
    static char_type const CHAR_LOWERCASE_R          = L'r';
    static char_type const CHAR_LOWERCASE_S          = L's';
    static char_type const CHAR_LOWERCASE_T          = L't';
    static char_type const CHAR_LOWERCASE_U          = L'u';
    static char_type const CHAR_LOWERCASE_V          = L'v';
    static char_type const CHAR_LOWERCASE_W          = L'w';
    static char_type const CHAR_LOWERCASE_X          = L'x';
    static char_type const CHAR_LOWERCASE_Y          = L'y';
    static char_type const CHAR_LOWERCASE_Z          = L'z';
    static char_type const CHAR_OPEN_CURLY__BRACE    = L'{';
    static char_type const CHAR_CLOSE_CURLY__BRACE   = L'}';
    static char_type const CHAR_DELETE               = L'\x7f';
};

template<>
struct ASCIIChars<char16_t>
{
    typedef wchar_t char_type;

    static char_type const CHAR_NULL                 = u'\0';
    static char_type const CHAR_SOH                  = u'\x01'; // Start of heading
    static char_type const CHAR_STX                  = u'\x02'; // start of text
    static char_type const CHAR_ETX                  = u'\x03'; // end of text
    static char_type const CHAR_EOT                  = u'\x04'; // end of transmission
    static char_type const CHAR_ENQ                  = u'\x05'; // enquiry
    static char_type const CHAR_ACK                  = u'\x06'; // acknowledge
    static char_type const CHAR_BEL                  = u'\x07'; // bell
    static char_type const CHAR_BS                   = u'\x08'; // backspace
    static char_type const CHAR_TAB                  = u'\x09'; // horizontal tab
    static char_type const CHAR_LF                   = u'\n';   // line feed/new line
    static char_type const CHAR_VT                   = u'\x0b'; // vertical tab
    static char_type const CHAR_FF                   = u'\x0c'; // NP form feed, new page
    static char_type const CHAR_CR                   = u'\r';   // carriage return
    static char_type const CHAR_SO                   = u'\x0e'; // shift out
    static char_type const CHAR_SI                   = u'\x0f'; // shift in
    static char_type const CHAR_DLE                  = u'\x10'; // data link escape
    static char_type const CHAR_DC1                  = u'\x11'; // device control 1
    static char_type const CHAR_DC2                  = u'\x12'; // device control 2
    static char_type const CHAR_DC3                  = u'\x13'; // device control 3
    static char_type const CHAR_DC4                  = u'\x14'; // device control 4
    static char_type const CHAR_NAK                  = u'\x15'; // negative acknowledge
    static char_type const CHAR_SYN                  = u'\x16'; // synchronous idle
    static char_type const CHAR_ETB                  = u'\x17'; // end of trans. block
    static char_type const CHAR_CAN                  = u'\x18'; // cancel
    static char_type const CHAR_EM                   = u'\x19'; // end of medium
    static char_type const CHAR_SUB                  = u'\x1a'; // substitute
    static char_type const CHAR_ESC                  = u'\x1b'; // escape
    static char_type const CHAR_FS                   = u'\x1c'; // file separator
    static char_type const CHAR_GS                   = u'\x1d'; // group separator
    static char_type const CHAR_RS                   = u'\x1e'; // record separator
    static char_type const CHAR_US                   = u'\x1f'; // unit separator
    static char_type const CHAR_SPACE                = u' ';
    static char_type const CHAR_EXCLAMATION_MARK     = u'!';
    static char_type const CHAR_QUOTATION_MARK       = u'\"';
    static char_type const CHAR_HASH                 = u'#';
    static char_type const CHAR_DOLLAR               = u'$';
    static char_type const CHAR_PERCENT              = u'%';
    static char_type const CHAR_AMPERSAND            = u'&';
    static char_type const CHAR_APOSTROPHE           = u'\'';
    static char_type const CHAR_OPEN_PARENTHESIS     = u'(';
    static char_type const CHAR_CLOSE_PARENTHESIS    = u')';
    static char_type const CHAR_ASTERISK             = u'*';
    static char_type const CHAR_PLUS                 = u'+';
    static char_type const CHAR_COMMA                = u',';
    static char_type const CHAR_HYPHEN               = u'-';
    static char_type const CHAR_PERIOD               = u'.';
    static char_type const CHAR_FORWARD_SLASH        = u'/';
    static char_type const CHAR_ZERO                 = u'0';
    static char_type const CHAR_ONE                  = u'1';
    static char_type const CHAR_TWO                  = u'2';
    static char_type const CHAR_THREE                = u'3';
    static char_type const CHAR_FOUR                 = u'4';
    static char_type const CHAR_FIVE                 = u'5';
    static char_type const CHAR_SIX                  = u'6';
    static char_type const CHAR_SEVEN                = u'7';
    static char_type const CHAR_EIGHT                = u'8';
    static char_type const CHAR_NINE                 = u'9';
    static char_type const CHAR_COLON                = u':';
    static char_type const CHAR_SEMICOLON            = u';';
    static char_type const CHAR_LESS_THAN            = u'<';
    static char_type const CHAR_EQUAL                = u'=';
    static char_type const CHAR_GREATER_THAN         = u'>';
    static char_type const CHAR_QUESTION_MARK        = u'?';
    static char_type const CHAR_AT                   = u'@';
    static char_type const CHAR_UPPERCASE_A          = u'A';
    static char_type const CHAR_UPPERCASE_B          = u'B';
    static char_type const CHAR_UPPERCASE_C          = u'C';
    static char_type const CHAR_UPPERCASE_D          = u'D';
    static char_type const CHAR_UPPERCASE_E          = u'E';
    static char_type const CHAR_UPPERCASE_F          = u'F';
    static char_type const CHAR_UPPERCASE_G          = u'G';
    static char_type const CHAR_UPPERCASE_H          = u'H';
    static char_type const CHAR_UPPERCASE_I          = u'I';
    static char_type const CHAR_UPPERCASE_J          = u'J';
    static char_type const CHAR_UPPERCASE_K          = u'K';
    static char_type const CHAR_UPPERCASE_L          = u'L';
    static char_type const CHAR_UPPERCASE_M          = u'M';
    static char_type const CHAR_UPPERCASE_N          = u'N';
    static char_type const CHAR_UPPERCASE_O          = u'O';
    static char_type const CHAR_UPPERCASE_P          = u'P';
    static char_type const CHAR_UPPERCASE_Q          = u'Q';
    static char_type const CHAR_UPPERCASE_R          = u'R';
    static char_type const CHAR_UPPERCASE_S          = u'S';
    static char_type const CHAR_UPPERCASE_T          = u'T';
    static char_type const CHAR_UPPERCASE_U          = u'U';
    static char_type const CHAR_UPPERCASE_V          = u'V';
    static char_type const CHAR_UPPERCASE_W          = u'W';
    static char_type const CHAR_UPPERCASE_X          = u'X';
    static char_type const CHAR_UPPERCASE_Y          = u'Y';
    static char_type const CHAR_UPPERCASE_Z          = u'Z';
    static char_type const CHAR_OPEN_SQUARE_BRACKET  = u'[';
    static char_type const CHAR_BACK_SLASH           = u'\\';
    static char_type const CHAR_CLOSE_SQUARE_BRACKET = u']';
    static char_type const CHAR_CARET                = u'^';
    static char_type const CHAR_UNDERSCORE           = u'_';
    static char_type const CHAR_GRAVE_ACCENT         = u'`';
    static char_type const CHAR_LOWERCASE_A          = u'a';
    static char_type const CHAR_LOWERCASE_B          = u'b';
    static char_type const CHAR_LOWERCASE_C          = u'c';
    static char_type const CHAR_LOWERCASE_D          = u'd';
    static char_type const CHAR_LOWERCASE_E          = u'e';
    static char_type const CHAR_LOWERCASE_F          = u'f';
    static char_type const CHAR_LOWERCASE_G          = u'g';
    static char_type const CHAR_LOWERCASE_H          = u'h';
    static char_type const CHAR_LOWERCASE_I          = u'i';
    static char_type const CHAR_LOWERCASE_J          = u'j';
    static char_type const CHAR_LOWERCASE_K          = u'k';
    static char_type const CHAR_LOWERCASE_L          = u'l';
    static char_type const CHAR_LOWERCASE_M          = u'm';
    static char_type const CHAR_LOWERCASE_N          = u'n';
    static char_type const CHAR_LOWERCASE_O          = u'o';
    static char_type const CHAR_LOWERCASE_P          = u'p';
    static char_type const CHAR_LOWERCASE_Q          = u'q';
    static char_type const CHAR_LOWERCASE_R          = u'r';
    static char_type const CHAR_LOWERCASE_S          = u's';
    static char_type const CHAR_LOWERCASE_T          = u't';
    static char_type const CHAR_LOWERCASE_U          = u'u';
    static char_type const CHAR_LOWERCASE_V          = u'v';
    static char_type const CHAR_LOWERCASE_W          = u'w';
    static char_type const CHAR_LOWERCASE_X          = u'x';
    static char_type const CHAR_LOWERCASE_Y          = u'y';
    static char_type const CHAR_LOWERCASE_Z          = u'z';
    static char_type const CHAR_OPEN_CURLY__BRACE    = u'{';
    static char_type const CHAR_CLOSE_CURLY__BRACE   = u'}';
    static char_type const CHAR_DELETE               = u'\x7f';
};

template<>
struct ASCIIChars<char32_t>
{
    typedef wchar_t char_type;

    static char_type const CHAR_NULL                 = U'\0';
    static char_type const CHAR_SOH                  = U'\x01'; // Start of heading
    static char_type const CHAR_STX                  = U'\x02'; // start of text
    static char_type const CHAR_ETX                  = U'\x03'; // end of text
    static char_type const CHAR_EOT                  = U'\x04'; // end of transmission
    static char_type const CHAR_ENQ                  = U'\x05'; // enquiry
    static char_type const CHAR_ACK                  = U'\x06'; // acknowledge
    static char_type const CHAR_BEL                  = U'\x07'; // bell
    static char_type const CHAR_BS                   = U'\x08'; // backspace
    static char_type const CHAR_TAB                  = U'\x09'; // horizontal tab
    static char_type const CHAR_LF                   = U'\n';   // line feed/new line
    static char_type const CHAR_VT                   = U'\x0b'; // vertical tab
    static char_type const CHAR_FF                   = U'\x0c'; // NP form feed, new page
    static char_type const CHAR_CR                   = U'\r';   // carriage return
    static char_type const CHAR_SO                   = U'\x0e'; // shift out
    static char_type const CHAR_SI                   = U'\x0f'; // shift in
    static char_type const CHAR_DLE                  = U'\x10'; // data link escape
    static char_type const CHAR_DC1                  = U'\x11'; // device control 1
    static char_type const CHAR_DC2                  = U'\x12'; // device control 2
    static char_type const CHAR_DC3                  = U'\x13'; // device control 3
    static char_type const CHAR_DC4                  = U'\x14'; // device control 4
    static char_type const CHAR_NAK                  = U'\x15'; // negative acknowledge
    static char_type const CHAR_SYN                  = U'\x16'; // synchronous idle
    static char_type const CHAR_ETB                  = U'\x17'; // end of trans. block
    static char_type const CHAR_CAN                  = U'\x18'; // cancel
    static char_type const CHAR_EM                   = U'\x19'; // end of medium
    static char_type const CHAR_SUB                  = U'\x1a'; // substitute
    static char_type const CHAR_ESC                  = U'\x1b'; // escape
    static char_type const CHAR_FS                   = U'\x1c'; // file separator
    static char_type const CHAR_GS                   = U'\x1d'; // group separator
    static char_type const CHAR_RS                   = U'\x1e'; // record separator
    static char_type const CHAR_US                   = U'\x1f'; // unit separator
    static char_type const CHAR_SPACE                = U' ';
    static char_type const CHAR_EXCLAMATION_MARK     = U'!';
    static char_type const CHAR_QUOTATION_MARK       = U'\"';
    static char_type const CHAR_HASH                 = U'#';
    static char_type const CHAR_DOLLAR               = U'$';
    static char_type const CHAR_PERCENT              = U'%';
    static char_type const CHAR_AMPERSAND            = U'&';
    static char_type const CHAR_APOSTROPHE           = U'\'';
    static char_type const CHAR_OPEN_PARENTHESIS     = U'(';
    static char_type const CHAR_CLOSE_PARENTHESIS    = U')';
    static char_type const CHAR_ASTERISK             = U'*';
    static char_type const CHAR_PLUS                 = U'+';
    static char_type const CHAR_COMMA                = U',';
    static char_type const CHAR_HYPHEN               = U'-';
    static char_type const CHAR_PERIOD               = U'.';
    static char_type const CHAR_FORWARD_SLASH        = U'/';
    static char_type const CHAR_ZERO                 = U'0';
    static char_type const CHAR_ONE                  = U'1';
    static char_type const CHAR_TWO                  = U'2';
    static char_type const CHAR_THREE                = U'3';
    static char_type const CHAR_FOUR                 = U'4';
    static char_type const CHAR_FIVE                 = U'5';
    static char_type const CHAR_SIX                  = U'6';
    static char_type const CHAR_SEVEN                = U'7';
    static char_type const CHAR_EIGHT                = U'8';
    static char_type const CHAR_NINE                 = U'9';
    static char_type const CHAR_COLON                = U':';
    static char_type const CHAR_SEMICOLON            = U';';
    static char_type const CHAR_LESS_THAN            = U'<';
    static char_type const CHAR_EQUAL                = U'=';
    static char_type const CHAR_GREATER_THAN         = U'>';
    static char_type const CHAR_QUESTION_MARK        = U'?';
    static char_type const CHAR_AT                   = U'@';
    static char_type const CHAR_UPPERCASE_A          = U'A';
    static char_type const CHAR_UPPERCASE_B          = U'B';
    static char_type const CHAR_UPPERCASE_C          = U'C';
    static char_type const CHAR_UPPERCASE_D          = U'D';
    static char_type const CHAR_UPPERCASE_E          = U'E';
    static char_type const CHAR_UPPERCASE_F          = U'F';
    static char_type const CHAR_UPPERCASE_G          = U'G';
    static char_type const CHAR_UPPERCASE_H          = U'H';
    static char_type const CHAR_UPPERCASE_I          = U'I';
    static char_type const CHAR_UPPERCASE_J          = U'J';
    static char_type const CHAR_UPPERCASE_K          = U'K';
    static char_type const CHAR_UPPERCASE_L          = U'L';
    static char_type const CHAR_UPPERCASE_M          = U'M';
    static char_type const CHAR_UPPERCASE_N          = U'N';
    static char_type const CHAR_UPPERCASE_O          = U'O';
    static char_type const CHAR_UPPERCASE_P          = U'P';
    static char_type const CHAR_UPPERCASE_Q          = U'Q';
    static char_type const CHAR_UPPERCASE_R          = U'R';
    static char_type const CHAR_UPPERCASE_S          = U'S';
    static char_type const CHAR_UPPERCASE_T          = U'T';
    static char_type const CHAR_UPPERCASE_U          = U'U';
    static char_type const CHAR_UPPERCASE_V          = U'V';
    static char_type const CHAR_UPPERCASE_W          = U'W';
    static char_type const CHAR_UPPERCASE_X          = U'X';
    static char_type const CHAR_UPPERCASE_Y          = U'Y';
    static char_type const CHAR_UPPERCASE_Z          = U'Z';
    static char_type const CHAR_OPEN_SQUARE_BRACKET  = U'[';
    static char_type const CHAR_BACK_SLASH           = U'\\';
    static char_type const CHAR_CLOSE_SQUARE_BRACKET = U']';
    static char_type const CHAR_CARET                = U'^';
    static char_type const CHAR_UNDERSCORE           = U'_';
    static char_type const CHAR_GRAVE_ACCENT         = U'`';
    static char_type const CHAR_LOWERCASE_A          = U'a';
    static char_type const CHAR_LOWERCASE_B          = U'b';
    static char_type const CHAR_LOWERCASE_C          = U'c';
    static char_type const CHAR_LOWERCASE_D          = U'd';
    static char_type const CHAR_LOWERCASE_E          = U'e';
    static char_type const CHAR_LOWERCASE_F          = U'f';
    static char_type const CHAR_LOWERCASE_G          = U'g';
    static char_type const CHAR_LOWERCASE_H          = U'h';
    static char_type const CHAR_LOWERCASE_I          = U'i';
    static char_type const CHAR_LOWERCASE_J          = U'j';
    static char_type const CHAR_LOWERCASE_K          = U'k';
    static char_type const CHAR_LOWERCASE_L          = U'l';
    static char_type const CHAR_LOWERCASE_M          = U'm';
    static char_type const CHAR_LOWERCASE_N          = U'n';
    static char_type const CHAR_LOWERCASE_O          = U'o';
    static char_type const CHAR_LOWERCASE_P          = U'p';
    static char_type const CHAR_LOWERCASE_Q          = U'q';
    static char_type const CHAR_LOWERCASE_R          = U'r';
    static char_type const CHAR_LOWERCASE_S          = U's';
    static char_type const CHAR_LOWERCASE_T          = U't';
    static char_type const CHAR_LOWERCASE_U          = U'u';
    static char_type const CHAR_LOWERCASE_V          = U'v';
    static char_type const CHAR_LOWERCASE_W          = U'w';
    static char_type const CHAR_LOWERCASE_X          = U'x';
    static char_type const CHAR_LOWERCASE_Y          = U'y';
    static char_type const CHAR_LOWERCASE_Z          = U'z';
    static char_type const CHAR_OPEN_CURLY__BRACE    = U'{';
    static char_type const CHAR_CLOSE_CURLY__BRACE   = U'}';
    static char_type const CHAR_DELETE               = U'\x7f';
};

} // namespace ocl

#endif // OCL_GUARD_CONST_ASCIICHARS_HPP
