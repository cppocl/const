# const

![](header_image.jpg)

## Overview

C++ constants.

## Code Examples

```cpp
#include "const/ASCIIChars.hpp"

int main()
{
    printf("Exclamation mark = %c\n", ASCIIChars<char>::CHAR_EXCLAMATION_MARK);
}
```
